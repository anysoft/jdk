#!/bin/sh
#####################
   auto install latest jdk of jdk8 >211
#####################
Type=jdk
Arc=x64
# Major=8
# Minor=311
# Build=b11


#originLists=`curl https://www.oracle.com/hk/java/technologies/javase/javase8u211-later-archive-downloads.html | grep "https://download.oracle.com/otn/java/jdk/8u"|awk -F"'" '{print $2}'|grep "download.oracle.com"`
originLists=`curl https://www.oracle.com/java/technologies/downloads/ | grep "https://download.oracle.com/otn/java/jdk/8u"|awk -F"'" '{print $2}'|grep "download.oracle.com"`
# https://download.oracle.com/otn/java/jdk/8u331-b09/165374ff4ea84ef0bbd821706e29b123/jdk-8u331-linux-aarch64.rpm
line=`echo ${originLists}|awk '{print $1}'`  
# 8u331-b09
version=`echo ${line}|awk -F "/" '{print $7}'`
# 165374ff4ea84ef0bbd821706e29b123
hash=`echo ${line}|awk -F "/" '{print $8}'`
# b09
Build=`echo ${version}|awk -F "-" '{print $2}'`
# 8
Major=`echo ${version}|awk -F "-" '{print $1}'|awk -F "u" '{print $1}'`
# 331
Minor=`echo ${version}|awk -F "-" '{print $1}'|awk -F "u" '{print $2}'`



Version=${Major}.${Minor}

TarFile=${Type}-${Major}u${Minor}-linux-${Arc}.tar.gz
Dir=${Type}1.${Major}.0_${Minor}

cd /usr/local
#url 登录获取后进入历史库供后续下载
wget "https://javadl.oracle.com/webapps/download/GetFile/1.${Major}.0_${Minor}-${Build}/${hash}/linux-i586/${TarFile}" \
    --no-cookies \
    --no-check-certificate \
    -O ${TarFile}

tar -xzvf ${TarFile}

mv ${Dir} java
echo "#JAVA HOME" >> /etc/profile
echo "export JAVA_HOME=/usr/local/java" >> /etc/profile
echo 'export JRE_HOME=${JAVA_HOME}/jre' >> /etc/profile
echo 'export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib' >> /etc/profile
echo 'export PATH=${JAVA_HOME}/bin:$PATH' >> /etc/profile
source /etc/profile > /dev/null 2>&1
echo ${Type} " install complete!"