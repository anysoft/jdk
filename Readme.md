# Orcale
## Java Archive List
https://www.oracle.com/java/technologies/downloads/archive/

## Java Standard Edition 8 Archive List

https://www.oracle.com/hk/java/technologies/javase/javase8u211-later-archive-downloads.html

## OverView
https://lv.binarybabel.org/api
```
# get the latest versoin or jdk download urls.
curl -s https://lv.binarybabel.org/catalog-api/java/jdk8.json
```

### mirror (from google. it may be not a security source)
https://enos.itcollege.ee/~jpoial/allalaadimised/jdk8/

## jdk-8u311

[jdk-8u311-linux-x64.rpm](http://download.oracle.com/otn-pub/java/jdk/8u311-b11/4d5417147a92418ea8b615e228bb6935/jdk-8u311-linux-x64.rpm)
[jdk-8u311-linux-x64.tar.gz](http://download.oracle.com/otn-pub/java/jdk/8u311-b11/4d5417147a92418ea8b615e228bb6935/jdk-8u311-linux-x64.tar.gz)
[jdk-8u311-macosx-x64.dmg](http://download.oracle.com/otn-pub/java/jdk/8u311-b11/4d5417147a92418ea8b615e228bb6935/jdk-8u311-macosx-x64.dmg)
[jdk-8u311-windows-x64.exe](http://download.oracle.com/otn-pub/java/jdk/8u311-b11/4d5417147a92418ea8b615e228bb6935/jdk-8u311-windows-x64.exe)

## gist from github
[gist](https://gist.github.com/wavezhang/ba8425f24a968ec9b2a8619d7c2d86a6)


# How to get the latest jdk?

uversion=331
buildno=b09
hash=165374ff4ea84ef0bbd821706e29b123

> use this [archive-downloads](https://www.oracle.com/hk/java/technologies/javase/javase8u211-later-archive-downloads.html  or https://www.oracle.com/java/technologies/downloads/) page to get the params of  `uversion`,`buildno`,`hash`
> to get the really download url you can replace the url below with these parameters.




# JDK8U331
## windows
### documents
https://javadl.oracle.com/webapps/download/GetFile/1.8.0_331-b09/165374ff4ea84ef0bbd821706e29b123/windows-i586/jdk-8u331-docs-all.zip

https://javadl.oracle.com/webapps/download/GetFile/1.8.0_331-b09/165374ff4ea84ef0bbd821706e29b123/windows-i586/jdk-8u331-windows-i586.exe
https://javadl.oracle.com/webapps/download/GetFile/1.8.0_331-b09/165374ff4ea84ef0bbd821706e29b123/windows-i586/jdk-8u331-windows-x64.exe


## macos
https://javadl.oracle.com/webapps/download/GetFile/1.8.0_331-b09/165374ff4ea84ef0bbd821706e29b123/unix-i586/jdk-8u331-macosx-x64.dmg

## linux
### aarch64
https://javadl.oracle.com/webapps/download/GetFile/1.8.0_331-b09/165374ff4ea84ef0bbd821706e29b123/linux-i586/jdk-8u331-linux-aarch64.rpm
https://javadl.oracle.com/webapps/download/GetFile/1.8.0_331-b09/165374ff4ea84ef0bbd821706e29b123/linux-i586/jdk-8u331-linux-aarch64.tar.gz
### arm32
https://javadl.oracle.com/webapps/download/GetFile/1.8.0_331-b09/165374ff4ea84ef0bbd821706e29b123/linux-i586/jdk-8u331-linux-arm32-vfp-hflt.tar.gz
### i586
https://javadl.oracle.com/webapps/download/GetFile/1.8.0_331-b09/165374ff4ea84ef0bbd821706e29b123/linux-i586/jdk-8u331-linux-i586.rpm
https://javadl.oracle.com/webapps/download/GetFile/1.8.0_331-b09/165374ff4ea84ef0bbd821706e29b123/linux-i586/jdk-8u331-linux-i586.tar.gz
### x64
https://javadl.oracle.com/webapps/download/GetFile/1.8.0_331-b09/165374ff4ea84ef0bbd821706e29b123/linux-i586/jdk-8u331-linux-x64.rpm
https://javadl.oracle.com/webapps/download/GetFile/1.8.0_331-b09/165374ff4ea84ef0bbd821706e29b123/linux-i586/jdk-8u331-linux-x64.tar.gz

## solaris
https://javadl.oracle.com/webapps/download/GetFile/1.8.0_331-b09/165374ff4ea84ef0bbd821706e29b123/solaris-i586/jdk-8u331-solaris-sparcv9.tar.Z
https://javadl.oracle.com/webapps/download/GetFile/1.8.0_331-b09/165374ff4ea84ef0bbd821706e29b123/solaris-i586/jdk-8u331-solaris-sparcv9.tar.gz
https://javadl.oracle.com/webapps/download/GetFile/1.8.0_331-b09/165374ff4ea84ef0bbd821706e29b123/solaris-i586/jdk-8u331-solaris-x64.tar.Z
https://javadl.oracle.com/webapps/download/GetFile/1.8.0_331-b09/165374ff4ea84ef0bbd821706e29b123/solaris-i586/jdk-8u331-solaris-x64.tar.gz








```

